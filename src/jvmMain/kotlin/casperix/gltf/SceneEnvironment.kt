package casperix.gltf

import casperix.gltf.render.RenderManager
import casperix.gltf.util.CameraUtil
import casperix.gltf.util.ImageBasedLighting
import casperix.gltf.util.SkyBox
import casperix.math.SphericalCoordinate
import casperix.scene.camera.orbital.OrbitalCameraInputSettings
import casperix.scene.camera.orbital.OrbitalCameraTransformSettings
import casperix.scene.camera.orbital.SimpleOrbitalCamera
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Cubemap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.glutils.FacedCubemapData
import com.badlogic.gdx.graphics.glutils.KTXTextureData
import kotlin.math.PI


object SceneEnvironment {
	fun default(renderManager: RenderManager) {
		orbitalCamera(renderManager)
		imageBasedLighting(renderManager)
		skybox(renderManager)
	}

	fun shadow(renderManager: RenderManager): casperix.exp.ShadowRender {
		val light = DirectionalLight()
		return casperix.exp.ShadowRender(renderManager, light)
	}

	fun orbitalCamera(renderManager: RenderManager, inputSettings: OrbitalCameraInputSettings = OrbitalCameraInputSettings(), transformSettings: OrbitalCameraTransformSettings = OrbitalCameraTransformSettings()): SimpleOrbitalCamera {
		val controller = CameraUtil.createCameraController(renderManager, inputSettings, transformSettings)
		controller.transformController.setOffset(SphericalCoordinate(10.0, PI / 4.0, 0.0))
		return controller
	}

	fun imageBasedLighting(renderManager: RenderManager) {
		ImageBasedLighting(
			Texture(Gdx.files.internal("environment/brdfLUT.png")),
			renderManager.environment,
			FacedCubemapData(
				Gdx.files.internal("environment/irradiance_posx.jpg"),
				Gdx.files.internal("environment/irradiance_negx.jpg"),
				Gdx.files.internal("environment/irradiance_posy.jpg"),
				Gdx.files.internal("environment/irradiance_negy.jpg"),
				Gdx.files.internal("environment/irradiance_posz.jpg"),
				Gdx.files.internal("environment/irradiance_negz.jpg"),
			),
			KTXTextureData(Gdx.files.internal("environment/radiance.ktx"), false)
		)
	}

	fun skybox(renderManager: RenderManager) {
		SkyBox(
			renderManager, Cubemap(
				FacedCubemapData(
					Gdx.files.internal("environment/skybox_posx.jpg"),
					Gdx.files.internal("environment/skybox_negx.jpg"),
					Gdx.files.internal("environment/skybox_posy.jpg"),
					Gdx.files.internal("environment/skybox_negy.jpg"),
					Gdx.files.internal("environment/skybox_posz.jpg"),
					Gdx.files.internal("environment/skybox_negz.jpg"),
				)
			)
		)
	}
}