package casperix.gltf.render

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.utils.RenderableSorter
import com.badlogic.gdx.utils.Array
import java.util.*
import kotlin.math.sign

class DummyRenderableSorter : RenderableSorter {
	override fun sort(camera: Camera?, renderables: Array<Renderable>?) {

	}
}