package casperix.gltf.render

import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider

class ModelBatchExt(provider:ShaderProvider) : ModelBatch(provider, DummyRenderableSorter()) {
	fun renderOptionalEnvironment(renderableProviders: Iterable<RenderableProvider>, environment: Environment) {
		for (renderableProvider in renderableProviders) renderOptionalEnvironment(renderableProvider, environment)
	}

	fun renderOptionalEnvironment(renderableProvider: RenderableProvider, environment: Environment) {
		val offset = renderables.size
		renderableProvider.getRenderables(renderables, renderablesPool)
		for (i in offset until renderables.size) {
			val renderable = renderables[i]
			renderable.environment = renderable.environment ?: environment
			renderable.shader = shaderProvider.getShader(renderable)
		}
	}
}