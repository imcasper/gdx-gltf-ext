package casperix.gltf.util

import casperix.gdx.geometry.toVector3
import casperix.gdx.input.Inputs
import casperix.gdx.input.registerInput
import casperix.gltf.render.RenderManager
import casperix.math.vector.Vector2i
import casperix.scene.camera.orbital.CameraSupport
import casperix.scene.camera.orbital.OrbitalCameraInputSettings
import casperix.scene.camera.orbital.OrbitalCameraTransformSettings
import casperix.scene.camera.orbital.SimpleOrbitalCamera
import casperix.signals.concrete.Promise
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera

object CameraUtil {
	fun createCameraController(renderManager: RenderManager, inputSettings: OrbitalCameraInputSettings = OrbitalCameraInputSettings(), cameraSettings: OrbitalCameraTransformSettings = OrbitalCameraTransformSettings()): SimpleOrbitalCamera {
		return createCameraController(renderManager.camera, renderManager.watcher.onUpdate, inputSettings, cameraSettings)
	}

	fun createCameraController(gdxCamera: Camera, nextFrame: Promise<Double>, inputSettings: OrbitalCameraInputSettings, transformSettings: OrbitalCameraTransformSettings): SimpleOrbitalCamera {
		val cameraInputs = Inputs()
		val orbitalCamera = SimpleOrbitalCamera(
			CameraSupport(nextFrame, { Vector2i(Gdx.graphics.width, Gdx.graphics.height) }, cameraInputs),
			inputSettings,
			transformSettings,
		) { transform ->
			val position = transform.position
			val forward = transform.getLocalY()
			val up = transform.getLocalZ()

			gdxCamera.position.set(position.toVector3())
			gdxCamera.up.set(up.toVector3())
			gdxCamera.direction.set(forward.toVector3())
			gdxCamera.update()
		}
		registerInput(cameraInputs)
		return orbitalCamera
	}
}