package casperix.gltf.util

import casperix.gdx.geometry.toVector3
import casperix.gdx.geometry.toVector3f
import casperix.gdx.graphics.GeometryBuilder
import casperix.gdx.graphics.addLine
import casperix.gltf.render.RenderManager
import casperix.math.geometry.Line3f
import casperix.math.vector.Vector3f
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder

class LightHelperRender(val renderManager: RenderManager) {
	private val RAY_LENGTH = 1000f
	private var lastInstance: ModelInstance? = null

	init {

		renderManager.onUpdate.then {
			lastInstance?.let {
				renderManager.remove(it)
			}

			val directionalLightsAttribute = renderManager.environment.get(DirectionalLightsAttribute.Type) as? DirectionalLightsAttribute ?: return@then
			val firstLight = directionalLightsAttribute.lights.firstOrNull() ?: return@then
			val target = firstLight.direction.toVector3f().normalize() * (-RAY_LENGTH)
			val lightModel = GeometryBuilder.run(vertexAttributesMask = VertexAttributes.Usage.Position or VertexAttributes.Usage.ColorUnpacked, figure = GeometryBuilder.GeometryType.LINES) { meshPartBuilder ->
				meshPartBuilder.addLine(Line3f(Vector3f.ZERO, target).convert { MeshPartBuilder.VertexInfo().setPos(it.toVector3d().toVector3()).setCol(1f, 1f, 0f, 1f) })
			}

			val nextInstance = ModelInstance(lightModel)
			renderManager.add(nextInstance)
			lastInstance = nextInstance
		}
	}
}