package casperix.gltf.scene

data class RenderableGroup(val renderColor: Boolean = true, val castShadow: Boolean = true, val priority:Int = 0)