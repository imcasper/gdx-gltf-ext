package casperix.exp.shader

data class ShaderEntryKey(val shadowCreationPhase: Boolean, val hasVertexNormal:Boolean, val hasVertexColor: Boolean, val hasShadowTexture:Boolean, val hasDiffuseTexture: Boolean, val hasNormalTexture: Boolean, val hasDiffuseColor: Boolean, val receiveShadow: Boolean, val pcf: Boolean, val hdr: Boolean, val gamma: Boolean, val blend:Boolean)