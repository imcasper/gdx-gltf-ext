package casperix.exp.shader.attribute

import com.badlogic.gdx.graphics.g3d.Attribute

data class ColorCorrectionAttribute(val hdr: Boolean, val gamma: Boolean) : Attribute(ID) {
	companion object {
		val ID = register("ColorCorrection")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return ColorCorrectionAttribute(hdr, gamma)
	}
}