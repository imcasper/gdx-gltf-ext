package casperix.exp.shader

import casperix.exp.shader.attribute.ShadowFactoryAttribute
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.DepthTestAttribute
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute
import com.badlogic.gdx.graphics.g3d.utils.RenderContext

class CustomShader(val provider: ShaderEntryProvider, val shadowCreationPhase: Boolean) : Shader {
	private var currentContext: RenderContext? = null
	private var currentEntry: ShaderEntry? = null
	private var currentAttributes:Attributes? = null
	private var currentMesh: Mesh? = null
	private var currentCamera: Camera? = null
	private val attributesAccumulator = ShaderAttributesAccumulator(::setupAttributes)

	override fun begin(camera: Camera, context: RenderContext) {
		currentCamera = camera
		currentContext = context
	}

	override fun render(renderable: Renderable) {
		attributesAccumulator.push(renderable)

		val entry = currentEntry!!
		val renderContext = currentContext!!
		entry.renderableLocations.forEach { it.set(renderContext, renderable) }

		val nextMesh = renderable.meshPart.mesh
		if (currentMesh != nextMesh) {
			nextMesh.bind(entry.program)
		}
		val size = if (nextMesh.numIndices != 0) nextMesh.numIndices else nextMesh.numVertices
		nextMesh.render(entry.program, renderable.meshPart.primitiveType, 0, size, false)
	}

	private fun setupAttributes(attributes: Attributes, vertexAttributes: VertexAttributes) {
		val camera = currentCamera!!
		val renderContext = currentContext!!

		val entry = provider.getOrCreateShader(shadowCreationPhase, attributes, vertexAttributes)
		if (currentEntry != entry) {
			currentEntry = entry

			val program = entry.program
			program.bind()

			entry.cameraLocations.forEach { it.set(renderContext, camera) }
			entry.attributesLocations.forEach { it.set(renderContext, attributes) }
		}

		if (currentAttributes?.compareTo(attributes) != 0) {
			currentAttributes = attributes
			val depthTestAttribute: DepthTestAttribute? = attributes.getAttribute(DepthTestAttribute.Type)
			if (depthTestAttribute == null) {
				renderContext.setDepthTest(GL20.GL_LEQUAL, 0f, 1f)
				renderContext.setDepthMask(true)
			} else {
				renderContext.setDepthTest(depthTestAttribute.depthFunc, depthTestAttribute.depthRangeNear, depthTestAttribute.depthRangeFar)
				renderContext.setDepthMask(depthTestAttribute.depthMask)
			}

			if (!shadowCreationPhase) {
				val blendingAttribute: BlendingAttribute? = attributes.getAttribute(BlendingAttribute.Type)
				if (blendingAttribute == null) {
					renderContext.setBlending(false, GL20.GL_ONE, GL20.GL_ONE)
				} else {
					renderContext.setBlending(blendingAttribute.blended, blendingAttribute.sourceFunction, blendingAttribute.destFunction)
				}
			}

			val cullFaceAttribute = (attributes.get(IntAttribute.CullFace) as? IntAttribute)?.value ?: GL20.GL_BACK


			if (shadowCreationPhase) {
				val shadowMaterialAttribute: ShadowFactoryAttribute? = attributes.getAttribute(ShadowFactoryAttribute.ID)
				if (shadowMaterialAttribute != null && !shadowMaterialAttribute.cast) return

				val shadowCullFace = when (cullFaceAttribute) {
					GL20.GL_FRONT_AND_BACK -> GL20.GL_FRONT_AND_BACK
					GL20.GL_FRONT -> GL20.GL_BACK
					GL20.GL_BACK -> GL20.GL_FRONT
					else -> cullFaceAttribute
				}
				renderContext.setCullFace(shadowCullFace)
			} else {
				renderContext.setCullFace(cullFaceAttribute)
			}

		}
	}

	override fun dispose() {}

	override fun init() {}

	override fun end() {
		currentEntry = null
		currentCamera = null
		currentContext = null
		attributesAccumulator.end()
	}

	override fun compareTo(other: Shader?): Int {
		if (other == null) return -1
		if (other === this) return 0
		return 1
	}

	override fun canRender(instance: Renderable?): Boolean {
		return true//instance?.shader == this
	}

}