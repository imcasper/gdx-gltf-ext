package casperix.exp.shader.location

import casperix.exp.shader.AbstractLocation
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Matrix4

class Matrix4Location<Source>(program: ShaderProgram, name: String, val provider: (source: Source) -> Matrix4?) : AbstractLocation<Source>(program, name) {

	override fun set(renderContext: RenderContext, source: Source) {
		if (locationId == -1) return
		val value = provider(source) ?: return
		program.setUniformMatrix(locationId, value)
	}
}