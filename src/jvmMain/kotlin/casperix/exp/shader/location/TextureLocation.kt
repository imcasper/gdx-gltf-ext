package casperix.exp.shader.location

import casperix.exp.shader.AbstractLocation
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor
import com.badlogic.gdx.graphics.glutils.ShaderProgram

class TextureLocation<Source>(program: ShaderProgram, name: String, val provider: (source:Source) -> TextureDescriptor<*>?) : AbstractLocation<Source>(program, name) {

	override fun set(renderContext: RenderContext, source:Source) {
		if (locationId == -1) return
		val textureDescriptor = provider(source) ?: return
		val textureId = renderContext.textureBinder.bind(textureDescriptor)
		if (textureId == -1) return
		program.setUniformi(locationId, textureId)
	}
}