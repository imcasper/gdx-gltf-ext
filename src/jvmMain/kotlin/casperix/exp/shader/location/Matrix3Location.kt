package casperix.exp.shader.location

import casperix.exp.shader.AbstractLocation
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Matrix3

class Matrix3Location<Source>(program: ShaderProgram, name: String, val provider: (source: Source) -> Matrix3?) : AbstractLocation<Source>(program, name) {

	override fun set(renderContext: RenderContext, source: Source) {
		if (locationId == -1) return
		val value = provider(source) ?: return
		program.setUniformMatrix(locationId, value)
	}
}