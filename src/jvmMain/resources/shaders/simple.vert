uniform mat4 projectionViewMatrix;
uniform mat4 modelMatrix;

in vec3 a_position;

void main()
{
    vec4 position = modelMatrix*vec4(a_position, 1.0);
    gl_Position = projectionViewMatrix*position;
}