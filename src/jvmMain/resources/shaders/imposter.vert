struct RangeInfo
{
    float lastFrame;
    float minAngle;
    float maxAngle;
};

uniform mat4 u_projViewTrans;
uniform vec3 u_cameraPosition;
uniform vec3 u_cameraDirection;
uniform vec2 u_scale;
uniform RangeInfo u_vertical;
uniform RangeInfo u_horizontal;

#ifdef shadowMapFlag
uniform mat4 u_shadowMapProjViewTrans;
out vec3 v_shadowMapUv;
#endif//shadowMapFlag

attribute vec3 a_position;
attribute vec2 a_offset;
attribute vec2 a_tex;

out vec2 v_tex;
out float v_layer;

const float PI = 3.14159265;
const float HALF_PI = 3.14159265 / 2.0;
const float DOUBLE_PI = 3.14159265 * 2.0;

void main()
{
    vec3 X = vec3(1, 0, 0);
    vec3 Z = vec3(0, 0, 1);

    vec3 objectToCamera =  u_cameraPosition - a_position;
    vec3 particleRight = -normalize(cross(objectToCamera, Z));
    vec3 particleUp = normalize(cross(particleRight, objectToCamera));

    vec3 position = a_position + particleRight * a_offset.x + particleUp * a_offset.y;
    vec4 vertexPosition = u_projViewTrans * vec4(position, 1);

    float cosVerticalAngle = dot(objectToCamera, Z) / length(objectToCamera);
    //  angle from Z to objectToCamera (0--PI)
    float verticalAngle =  acos(cosVerticalAngle);
    //  horizontal angle (0--2PI)
    float horizontalAngle =  PI - atan(objectToCamera.y, objectToCamera.x);

    float verticalAngleNormalized = (verticalAngle  - u_vertical.minAngle) /(u_vertical.maxAngle - u_vertical.minAngle);
    float verticalStep = clamp(round(verticalAngleNormalized * u_vertical.lastFrame), 0.0, u_vertical.lastFrame);

    float horizontalAngleNormalized = (horizontalAngle - u_horizontal.minAngle) / (u_horizontal.maxAngle - u_horizontal.minAngle);
    float horizontalStep = clamp(round(horizontalAngleNormalized * u_horizontal.lastFrame), 0.0, u_horizontal.lastFrame);

    v_tex = a_tex;
    v_layer = (u_horizontal.lastFrame + 1.0) * verticalStep + horizontalStep;

    #ifdef shadowMapFlag
    vec4 shadowPosition = u_shadowMapProjViewTrans * vec4(position, 1);
    v_shadowMapUv.xyz = (shadowPosition.xyz / shadowPosition.w) * 0.5 + 0.5;
    v_shadowMapUv.z = min(v_shadowMapUv.z, 0.998);
    #endif//shadowMapFlag
    gl_Position =  vertexPosition;
}