uniform sampler2D u_albedo_texture;

varying vec2 v_texCoord;

void main()
{
    #ifdef ALPHA_BLEND_TEXTURE
    vec4 sourceColor = texture(u_albedo_texture, v_texCoord);
    if (sourceColor.a < 0.5) discard;
    #endif


    gl_FragColor.x = gl_FragCoord.z;
}