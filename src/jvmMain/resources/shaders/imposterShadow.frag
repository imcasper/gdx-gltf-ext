#ifdef GL_ES
#define LOWP lowp
#define MED mediump
#define HIGH highp
precision mediump float;
#else
#define MED
#define LOWP
#define HIGH
#endif

uniform sampler2DArray u_textureArray;

varying HIGH float v_depth;
varying vec2 v_tex;
varying float v_layer;

void main()
{
    float minLayer = floor(v_layer);
    float maxLayer = floor(v_layer + 1.0);
    float maxWeight = v_layer - minLayer;
    float minWeight = 1.0 - maxWeight;
    vec4 textureColor = texture(u_textureArray, vec3(v_tex, minLayer)) * (minWeight) + texture(u_textureArray, vec3(v_tex, maxLayer)) * (maxWeight);
    if (textureColor.a < 0.3) discard;

    HIGH float depth = v_depth;
    const HIGH vec4 bias = vec4(1.0 / 255.0, 1.0 / 255.0, 1.0 / 255.0, 0.0);
    HIGH vec4 color = vec4(depth, fract(depth * 255.0), fract(depth * 65025.0), fract(depth * 16581375.0));
    gl_FragColor = color - (color.yzww * bias);

}