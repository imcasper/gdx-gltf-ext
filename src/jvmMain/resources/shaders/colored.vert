uniform mat4 projectionViewMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;
uniform mat4 lightSpaceMatrix;
uniform vec4 diffuseColor;

varying mat3 v_normal_mat;

in vec3 a_position;
in vec4 a_color;
in vec3 a_normal;
in vec2 a_texCoord0;
in vec3 a_binormal;
in vec3 a_tangent;

out VS_OUT {
    vec3 position;
    vec3 normal;
    vec2 texCoord;
    vec4 positionInLightSpace;
    vec4 color;
} vs;

void main()
{
    v_normal_mat[0] = a_tangent;
    v_normal_mat[1] = cross(a_normal, a_tangent);
    v_normal_mat[2] = a_normal;

    vec4 position = modelMatrix*vec4(a_position, 1.0);
    gl_Position = projectionViewMatrix*position;

    #ifdef DIFFUSE_COLOR
    vs.color = diffuseColor * a_color;
    #else
    vs.color = a_color;
    #endif
    vs.position = position.xyz;
    vs.position = position.xyz;
    vs.normal = normalMatrix * a_normal;
    vs.texCoord = a_texCoord0;
    vs.positionInLightSpace = lightSpaceMatrix * position;
}