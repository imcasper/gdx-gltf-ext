package casperix.gltf

import casperix.gdx.app.DesktopApplicationLauncher
import casperix.gltf.loader.GLTFLoaderExt
import casperix.gltf.render.RenderManager
import com.badlogic.gdx.graphics.g3d.ModelInstance
import org.junit.Before
import org.junit.Test


class SimpleDemo {

	@Before
	fun start() {
		DesktopApplicationLauncher() {
			val renderManager = RenderManager.createPBRender(it)
			val testModel = GLTFLoaderExt.loadModel("test.gltf")
			renderManager.add(ModelInstance(testModel))
		}
	}

	@Test
	fun dummy() {

	}
}

